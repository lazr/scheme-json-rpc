# An implementation of the JSON-RPC for Scheme


## Introduction

*EXPERIMENTAL*

The JSON-RPC allows calling procedures on remote servers by
exchanging JSON objects. It's specification can be found at
[https://www.jsonrpc.org/specification](https://www.jsonrpc.org/specification).

It currently supports CHICKEN 5 and Guile 3.

## Installing

### CHICKEN
This library is available as an .egg. Install it by running:
```
chicken-install -s json-rpc
```

<!-- ### Gambit -->
<!-- Starting with Gambit 4.9.4, you can use the built-in package manager to install -->
<!-- this library. Since this library name does not contain the repository path (it -->
<!-- would be a hassle to change this for the other implementations at this point), -->
<!-- some extra manual steps are needed to get it installed. -->

<!-- 1. Download and decompress a copy of the repository to some path. You can -->
<!-- download the master branch here: https://codeberg.org/rgherdt/scheme-json-rpc/archive/master.zip -->

<!-- 2. Assuming the repository is decompressed to `~/src/json-rpc`, change to -->
<!-- `~/src` and install `json-rpc` like so: -->

<!-- ``` -->
<!-- $ cd ~/src -->
<!-- $ gsi -install json-rpc -->
<!-- ``` -->

<!-- You may also compile the modules running: -->
<!-- ``` -->
<!-- gsc json-rpc/gambit json-rpc/private json-rpc/lolevel json-rpc -->
<!-- ``` -->

<!-- *Note: Unfortunately I named the original repository with a different name as -->
<!--  the library. Make sure the decompressed directory is named json-rpc, and not scheme-json-rpc, -->
<!--  otherwise installation will fail.* -->

### Guile
You can install the library for Guile executing following commands in the `guile`
subfolder:

```
cd guile/
./configure
make
sudo make install
```

## Documentation

See [http://wiki.call-cc.org/eggref/5/json-rpc](http://wiki.call-cc.org/eggref/5/json-rpc).

## LICENSE

MIT License

Copyright (c) 2021 Ricardo G. Herdt

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
