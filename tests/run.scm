(cond-expand
 (chicken (import (scheme base)
                  (json-rpc chicken)
                  (srfi 28)
                  (srfi 64)))
 (guile (import (except (scheme base)
                        cond-expand
                        error
                        for-each
                        assoc
                        include
                        raise)
                (json-rpc guile)
                (srfi srfi-18)
                (srfi srfi-64)))
 (gambit (import (json-rpc gambit)
                 (_test))))

(cond-expand
 (gambit (import (only (srfi 13) string-trim-right)))
 (else))

(import (json-rpc)
        (json-rpc lolevel))

(json-rpc-log-level 'error)
(define server-in-file (make-parameter #f))
(define server-out-file (make-parameter #f))

(cond-expand
 (gambit (define json-rpc-respond
           json-rpc/lolevel#json-rpc-respond))
 (guile (define json-rpc-respond
          (@@ (json-rpc lolevel) json-rpc-respond)))
 (chicken (define json-rpc-respond
            json-rpc.lolevel#json-rpc-respond)))

(define (process-request in-port out-port)
  (parameterize
      ((json-rpc-handler-table
        `(("hello" . ,(lambda (params)
                        (let ((name (cdr (assoc 'name params))))
                          (string-append "Hello " name))))
          ("subtract" . ,(lambda (params)
                            (- (vector-ref params 0)
                               (vector-ref params 1))))
          ("update" . ,(lambda (params)
                         #f))
          ("crash" . ,(lambda (params)
                        (raise (make-json-rpc-internal-error
                                "Crash"))))
          ("exit" . ,(lambda (params)
                       (json-rpc-exit)
                       #f)))))
    (let ((req (json-rpc-read in-port)))
      (json-rpc-respond req out-port))))

(define (run-test method params expected-result notification?)
  (define client-out-port (open-output-string))
  (define server-out-port (open-output-string))

  (json-rpc-write `((id . 0)
                    (method . ,method)
                    (params . ,params))
                  client-out-port)

  (let* ((client-out-str (get-output-string client-out-port))
         (server-in-port (open-input-string client-out-str)))
    (process-request server-in-port server-out-port)

    (let* ((res-str (get-output-string server-out-port))
           (client-in-port (open-input-string res-str))
           (res (json-rpc-read client-in-port)))
      (cond (notification? #t)
            ((and expected-result res)
             (test-equal expected-result (alist-ref 'result res))
             (test-eq #f (alist-ref 'error res)))
            (else
             (test-assert (alist-ref 'error res))
             (test-eq #f (alist-ref 'result res))))
      (close-input-port client-in-port)
      (close-input-port server-in-port)
      (close-output-port client-out-port)
      (close-output-port server-out-port))))

(test-begin "json-rpc tests")

(run-test "hello" '((name . "World!")) "Hello World!" #f)
(run-test "hellow" '((name . "World!")) #f #f)
(run-test "crash" '((name . "World!")) #f #f)
(run-test "subtract" #(42 23) 19 #f)
(run-test "subtract" #(23 42) -19 #f)
(run-test "update" #(1 2 3 4 5) #f #t)

(test-end "json-rpc tests")

