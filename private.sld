(define-library (json-rpc private)

(export json-rpc-log-file
        json-rpc-log-level
        satisfies-log-level?
        write-log)

(import (scheme base)
        (scheme file)
        (scheme write))

(cond-expand (gambit (import (only (srfi 13) string-upcase)
                             (srfi 28)))
             (guile (import (srfi srfi-13)
                               (srfi srfi-28)))
             (chicken (import (only (srfi 13) string-upcase)
                              (srfi 28))))

(include "private.scm"))
