(define-library (json-rpc lolevel)

(export json-rpc-exit
        json-rpc-handler-table
        json-rpc-log-level
        json-rpc-loop
        json-rpc-read
        json-rpc-write

        custom-error-codes
        make-json-rpc-custom-error
        make-json-rpc-internal-error
        make-json-rpc-invalid-request-error
        json-rpc-error?
        json-rpc-custom-error?
        json-rpc-invalid-request-error?
        json-rpc-internal-error?)

(import (scheme base)
        (scheme char)
        (json-rpc private)
        (srfi 28)
        (srfi 69))

(cond-expand
 (chicken
  (import (json-rpc chicken)
          (only (srfi 13)
                string-take
                string-trim
                string-trim-right)))
 (gambit
  (import (json-rpc gambit)
          (only (srfi 13)
                string-take
                string-trim
                string-trim-right))))

(include "lolevel.scm"))
