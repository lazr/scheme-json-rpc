(define-library (json-rpc)

(export json-rpc-call
        json-rpc-call/tcp
        json-rpc-log-file
        json-rpc-log-level
        json-rpc-exit
        json-rpc-handler-table
        json-rpc-send-request
        json-rpc-send-notification
        json-rpc-start-server/tcp

        define-notification-handler
        define-request-handler)

(cond-expand
 (chicken (import (chicken tcp)
                  (srfi 180)
                  (r7rs)))
 (else))

(import (scheme base)
        (scheme case-lambda)
        (scheme char)
        (scheme write)
        (json-rpc private)
        (json-rpc lolevel))

(cond-expand (guile (import (srfi srfi-28)))
             (else (import (srfi 28))))

(cond-expand
 (chicken (import (only (chicken condition) print-error-message)))
 (gambit (import (json-rpc gambit))))

(include "json-rpc.scm"))
