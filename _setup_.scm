(define-module-alias (chibi irregex)
  (github.com/ashinn/irregex irregex))

(define-module-alias (srfi 145)
  (codeberg.org/rgherdt/srfi srfi 145))

(define-module-alias (srfi 180)
  (codeberg.org/rgherdt/srfi srfi 180))

(define-module-alias json-rpc
  (codeberg.org/rgherdt/scheme-json-rpc))
