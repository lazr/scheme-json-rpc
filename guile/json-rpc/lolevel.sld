(define-module (json-rpc lolevel)

#:export (json-rpc-exit
          json-rpc-handler-table
          json-rpc-loop
          json-rpc-read
          json-rpc-write

          custom-error-codes
          make-json-rpc-custom-error
          make-json-rpc-internal-error
          make-json-rpc-invalid-request-error
          json-rpc-error?
          json-rpc-custom-error?
          json-rpc-internal-error?
          json-rpc-invalid-request-error?)

#:re-export (json-rpc-log-level)

#:use-module ((scheme base)
              #:select (define-record-type
                        eof-object
                        flush-output-port
                        guard
                        read-bytevector
                        read-error?
                        read-line
                        textual-port?
                        utf8->string
                        vector-for-each
                        write-string))
#:use-module (scheme char)
#:use-module ((scheme inexact)
              #:select (infinite?))
#:use-module (scheme case-lambda)
#:use-module (scheme write)
#:use-module (srfi srfi-1)
#:use-module (srfi srfi-13)
#:use-module (srfi srfi-18)
#:use-module (srfi srfi-28)
#:use-module (srfi srfi-60)
#:use-module (srfi srfi-69)
#:use-module (json-rpc private)
#:use-module (json-rpc guile)

#:declarative? #f)
