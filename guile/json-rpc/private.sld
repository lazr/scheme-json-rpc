(define-module (json-rpc private)

#:export (json-rpc-log-file
          json-rpc-log-level
          satisfies-log-level?
          write-log)

#:use-module ((scheme base) #:select (call-with-port flush-output-port))
#:use-module (scheme write)
#:use-module (srfi srfi-13)
#:use-module (srfi srfi-28)

#:declarative? #f)

