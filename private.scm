(define json-rpc-log-level (make-parameter 'debug))
(define json-rpc-log-file (make-parameter #f))

(define (get-log-level symb)
  (cond ((eqv? symb 'silent) -1)
        ((eqv? symb 'error) 0)
        ((eqv? symb 'warning) 1)
        ((eqv? symb 'info) 2)
        ((eqv? symb 'debug) 3)
        (else (error "invalid log level" symb))))

(define (satisfies-log-level? target-level)
  (>= (get-log-level (json-rpc-log-level))
      (get-log-level target-level)))

(define (write-log target-level msg . args)
  (define (print-log port)
    (when (satisfies-log-level? target-level)
      (display (format "[JSON-RPC] ~a: ~a"
                       (string-upcase (symbol->string target-level))
                       msg)
               port)
      (when (not (null? args))
        (display ": " port)
        (map (lambda (s)
               (display (format "~a    " s) port))
             args))
      (newline port)
      (flush-output-port port)))
  (cond ((json-rpc-log-file)
         => (lambda (fname)
              (cond-expand
               (chicken (call-with-output-file fname
                          (lambda (port)
                            (print-log port))
                          #:append))
               (guile (call-with-port (open-file fname "a")
                                      (lambda (port)
                                        (print-log port))))
               (else (call-with-output-file fname
                       (lambda (port)
                         (print-log port)))))))
        (else (print-log (current-error-port)))))
