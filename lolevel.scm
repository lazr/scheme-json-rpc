

(define core-error-codes
  '((parse-error . -32700)
    (invalid-request . -32600)
    (method-not-found . -32601)
    (invalid-parameters . -32602)
    (internal-error . -32603)))

(define custom-error-codes
  (make-parameter '()))

(define (error-codes)
  (append core-error-codes (custom-error-codes)))

(define error-code-to-symbol-map
  (map (lambda (p)
         (cons (cdr p) (car p)))
       (error-codes)))

(define json-rpc-handler-table
  (make-parameter '() (lambda (alist)
                        (if (null? alist)
                            ;; due to a bug in Gambit 4.9.4, alist->hash-table
                            ;; over the empty list crashes. Fixed on master.
                            (make-hash-table)
                            (alist->hash-table alist equal?)))))

(define (json-rpc-loop in-port out-port)
  (write-log 'debug "json-rpc-loop started")
  (guard
   (condition
    ((eq? condition 'json-rpc-exit)
     (write-log 'info "Closing JSON-RPC server.")
     'json-rpc-exit)
    (#t (raise condition)))
   (let loop ((request (json-rpc-read in-port)))
     (write-log 'debug
                (truncate-string
                 (format "received request: ~a" request)))
     (cond ((eof-object? request)
            (write-log 'info "connection closed on other end-point\r\n")
            #f)
           ((not request)
            (write-log 'warning
                       (format "unknown command: ~a~%" request))
            (loop (json-rpc-read in-port)))
           (else
            (write-log 'debug
                       (truncate-string
                        (format "received JSON: ~a~%"
                                request)))
            (json-rpc-respond request out-port)
            (loop (json-rpc-read in-port)))))))

(define (json-rpc-read in-port)
  (define line (read-line in-port))
  (if (eof-object? line)
      (eof-object)
      (let* ((request (string-trim line)))
        (write-log 'debug
                   (truncate-string
                    (format "REQUEST ~a~%"
                            request)))

        (cond ((eof-object? request)
               (write-log 'debug "<eof>\r\n")
               request)
              ((< (string-length request) 17)
               (write-log 'warning
                          (format "unknown command: ~a~%" request))
               #f)
              (else
               (let ((header-prefix (substring request 0 16)))
                 (cond ((string=? header-prefix "Content-Length: ")
                        (let ((num (string->number
                                    (string-trim-right
                                     (substring request 16 (string-length request))))))
                          (write-log
                           'debug
                           (format "Receiving input of length ~a~%" num))
                          (json-read in-port)))
                       (else (write-log 'error "ill-formed header" request)
                             #f))))))))

(define (json-rpc-write scm . args)
  (define str ((scheme->json-string) scm))
  (define port
    (if (not (null? args))
        (car args)
        (current-output-port)))
  (write-string (format "Content-Length: ~a\r\n\r\n"
                        (+ 4 (string-length str)))
                port)
  (write-string str port)
  (write-string "\r\n" port)
  (write-string "\r\n" port)
  (flush-output-port port))

(define (json-rpc-respond request out-port)
  (define response (json-rpc-compute-response request))
  (cond (response
         (write-log 'debug "json-rpc-respond: responding with: ")
         (write-log 'debug
                    (with-output-to-string
                      (lambda ()
                        (json-rpc-write response))))
         (json-rpc-write response out-port))
        (else
         (write-log 'debug
                    (truncate-string
                     (format "no response for request ~a" request))))))

(define (json-rpc-compute-response request)
  (define id (alist-ref 'id request))
  (guard (condition
          ((json-rpc-error? condition)
           (write-log 'debug (format "~a" condition))
           (make-response id
                          #f
                          (cdr condition)))
          (#t (write-log 'error (format "json-rpc-compute-response: unknown error: ~a"
                                        condition))
              (make-response id
                             #f
                             (format "~a" condition))))
    (json-rpc-dispatch request)))

(define (json-rpc-dispatch request)
  (define (dispatch j)
    (let* ((method-pair (assoc 'method j))
           (params-pair (assoc 'params j)))
      (unless (and method-pair params-pair)
        (raise (make-json-rpc-invalid-request-error)))
      (let* ((method (hash-table-ref/default
                      (json-rpc-handler-table)
                      (cdr method-pair)
                      #f)))
        (unless method
          (raise (make-json-rpc-method-not-found-error (cdr method-pair))))
        (if params-pair
            (let ((params (extract-params params-pair)))
              (method params))
            (method)))))
  (define id (assoc 'id request))
  (let ((result (dispatch request)))
    (cond ((not result) #f)
          (id (make-response (cdr id)
                             result
                             #f))
          (else #f))))

(define (json-rpc-exit)
  (raise 'json-rpc-exit))

(define (make-response id result err)
  (let ((content (if err
                     (cons 'error err)
                     (cons 'result result))))
    (if id
        `((jsonrpc . "2.0")
          (id . ,id)
          ,content)
        `((jsonrpc . "2.0")
          (id . null)
          ,content))))

(define (extract-params params-pair)
  (cdr params-pair))

(define-syntax define-json-rpc-error
  (syntax-rules ()
    ((define-json-rpc-error ctor pred error-symbol msg)
     (begin
       (define (ctor . args)
         (let ((full-msg (if (or (null? args)
                                 (not (car args)))
                             msg
                             (string-append msg ": " (car args)))))
           (cons error-symbol
                 `((message . ,full-msg)
                   (code . ,(alist-ref error-symbol
                                       (error-codes)))))))
       (define (pred condition)
         (and (pair? condition)
              (eqv? (car condition) error-symbol)))))))

(define-json-rpc-error
  make-json-rpc-internal-error
  json-rpc-internal-error?
  'internal-error
  "Internal error")

(define-json-rpc-error
  make-json-rpc-invalid-request-error
  json-rpc-invalid-request-error?
  'invalid-request
  "Invalid request error")

(define-json-rpc-error
  make-json-rpc-method-not-found-error
  json-rpc-method-not-found-error?
  'method-not-found
  "Method not found")

(define (make-json-rpc-custom-error error-symbol msg)
  (cons error-symbol
        `((message . ,msg)
          (code . ,(alist-ref error-symbol (custom-error-codes))))))

(define (json-rpc-custom-error? condition)
  (and (pair? condition)
       (let ((tag (car condition)))
         (member tag
                 (map car (custom-error-codes))))))

(define (json-rpc-error? condition)
  (and (pair? condition)
       (let ((tag (car condition)))
         (member tag
                 (map car (error-codes))))))

(define (json-rpc-error-contents err)
  (cdr err))

(define (truncate-string str)
  (define max-length 150)
  (if (< (string-length str) max-length)
      str
      (string-append (string-take str max-length) " ...")))
