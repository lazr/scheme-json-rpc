(define-library (json-rpc chicken)

(export alist-ref
        json-read
        scheme->json-string
        with-output-to-string)

(import (only (chicken base)
              alist-ref)
        (only (chicken port)
              call-with-output-string
              with-output-to-string)
        (srfi 180)
        r7rs
        scheme)


(begin
  (define scheme->json-string
    (make-parameter (lambda (scm)
                      (call-with-output-string
                       (lambda (p) (json-write scm p))))))))
